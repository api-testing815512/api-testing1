 Project Name: API testing using Postman

Project Description:Postman has a variety of tools, views, and controls to help you manage your API projects.It used to developers to test, document,and share APIs (Application Programming Interfaces).



Steps:
1.Configure API using diff methods in postman .In that we use (GET POST PUT PATCH DELETE)methods.
GET :GET methods are typically for retrieving data from an API
POST : This request method used to create new task or add new data.
PUT: This request method used to replace existing data.
PATCH: This request method used to update some existing data fields.
DELETE:This request method used to Erase data permanently.


2.Data Driven Testing:
Data-driven testing can be a very effective approach in testing an API against different data-sets. Postman supports CSV and JSON files to get data for the test scripts. The data-driven approach is useful when we need to execute a test with multiple sets of Data. Also, modifying or adding any data fields will just need updating the data files which is easier than going to test script and updating test data.

2.1 Using Random test data.

2.2 Reading data from Excel file.
Here We create Data filee in Excel file then upload in collection runner and test it.

2.3 Reading data from JSON file.
we Create JSON data file in given format then upload in collection runner and test it.

3.Retry API in postman :
In our test environment, unfortunately quite some requests are failing from time to time, due to timeouts, environment not being available this requests a retry API is used .It performs fixed iteration automatically.

Retry API in postman for Fixed iterations :
This collection shows how we can run loop over the same request while the given condition is true. if the given condition is wrong at that time its fails or stop performing request.

Retry API in postman for Multiple iterations for same request automatically.
This collection shows how we can run loop over the same request while changing the parameters using the Collection Runner and the postman. setNextRequest() function.

4.Create html report using newmen.
Postman generates html reports that enable to visualize data .These reports gives information about  the state of your APIs, including tests, documentation, and monitoring coverage.
Download the newmen and install in our system . using diff syntax we can create different html reports:

Syntax:
newman run {location of postman collection \ name of the collection } -d{location of the data file and name of the data file } -e {location of postman environment  \ name of the environment } --reporters {name of the reporter} --reporter-{ name of the reporter }-export {location where you want save your out put html report}






